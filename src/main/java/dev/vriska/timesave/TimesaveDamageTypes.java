package dev.vriska.timesave;

import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.DamageType;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class TimesaveDamageTypes {
    public static final RegistryKey<DamageType> TIMESAVE = RegistryKey.of(RegistryKeys.DAMAGE_TYPE,
                                                                          new Identifier("timesave", "timesave"));

    public static DamageSource of(World world, RegistryKey<DamageType> key) {
        return new DamageSource(world.getRegistryManager().get(RegistryKeys.DAMAGE_TYPE).entryOf(key));
    }
}
