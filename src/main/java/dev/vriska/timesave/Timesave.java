package dev.vriska.timesave;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Timesave implements ModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("timesave");

    @Override
    public void onInitialize() {

    }
}
