package dev.vriska.timesave.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.DeathScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableTextContent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(DeathScreen.class)
public abstract class DeathScreenMixin extends Screen {
    protected DeathScreenMixin(Text title) {
        super(title);
    }

    @Accessor
    abstract Text getMessage();

    @Accessor
    abstract boolean getIsHardcore();

    @ModifyArg(method = "init", at = @At(value = "INVOKE", target = "Lnet/minecraft/text/Text;translatable" +
        "(Ljava/lang/String;)Lnet/minecraft/text/MutableText;"), index = 0)
    private String modifyText(String text) {
        if (getMessage() != null && getMessage().getContent() instanceof TranslatableTextContent x) {
            if (x.getKey().equals("death.attack.timesave") || x.getKey().equals("death.attack.timesave.player") ||
                x.getKey().equals("deathScreen.backToTheStage")) {
                if (text.equals("deathScreen.respawn")) {
                    return "deathScreen.startAgain";
                } else if (text.equals("deathScreen.titleScreen")) {
                    return "deathScreen.quit";
                }
            }
        }
        return text;
    }

    @Inject(method = "onTitleScreenButtonClicked", at = @At(value = "HEAD"), cancellable = true)
    private void onTitleScreenButtonClicked(CallbackInfo ci) {
        if (getMessage() != null && getMessage().getContent() instanceof TranslatableTextContent x) {
            if (x.getKey().equals("death.attack.timesave") || x.getKey().equals("death.attack.timesave.player") ||
                x.getKey().equals("deathScreen.backToTheStage")) {
                DeathScreen newScreen = new DeathScreen(Text.translatable("deathScreen.backToTheStage"),
                                                        getIsHardcore());
                this.client.setScreen(newScreen);
                ci.cancel();
            }
        }
    }
}
